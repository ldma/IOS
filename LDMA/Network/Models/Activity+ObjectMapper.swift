//
//  Activity.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 11/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import ObjectMapper

extension Activity: Mappable {   
    
    public func mapping(map: Map) {
        id <- map["id"]
        company_id <- map["company_id"]
        owner_id <- map["owner_id"]
        owner <- map["owner"]
        service_id <- map["service_id"]
        name <- map["name"]
        description <- map["description"]
        isPrivate <- map["isPrivate"]
        init_date <- map["init_date"]
        end_date <- map["end_date"]
        invited <- map["invited"]
        latitude <- map["latitude"]
        longitude <- map["longitude"]
    }
}
