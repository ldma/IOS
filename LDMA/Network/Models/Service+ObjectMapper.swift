//
//  Service+ObjectMapper.swift
//  LDMA
//
//  Created by Alberto Galera Sánchez on 04/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import ObjectMapper

extension Service : Mappable {
    
    func mapping(map: Map) {
        id  <- map["id"]
        name <- map["name"]
        provider <- map["provider"]
        description <- map["description"]
        thumb_url <- map["thumb_url"]
        cover_url <- map["cover_url"]
        gps_lat  <- map["gps_lat"]
        gps_long <- map["gps_long"]
    }
    
}
