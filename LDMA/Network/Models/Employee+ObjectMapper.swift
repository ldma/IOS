//
//  Employee+ObjectMapper.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 09/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import ObjectMapper

extension Employee: Mappable {
    
    public func mapping(map: Map) {
        id <- map["id"]
        company_id <- map["company_id"]
        name <- map["name"]
        surnames <- map["surnames"]
        email <- map["email"]
        avatar_url <- map["avatar_url"]
    }
    
}
