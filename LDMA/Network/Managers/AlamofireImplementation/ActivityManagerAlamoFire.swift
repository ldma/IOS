//
//  ActivityNetworkModel.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 11/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import ObjectMapper
import Alamofire
import RxAlamofire
import RxCocoa
import RxSwift
import KVLoading
import CRNotifications

struct ActivityManagerAlamoFire: ListProtocol, SaveProtocol, DeleteProtocol{
    
    func list<Activity: Mappable>() -> Observable<[Activity]> {
        KVLoading.show()
        
        var queryStrings = QueryStrings()
        queryStrings["company_id"] = "1" //TODO: add employee company
        queryStrings["limit"] = "100" //TODO: Infinite Scroll!!
        queryStrings["init_date"] = Date.actualDate()
        
        let request = ApiRequest.init(finalEndpoint: "activity", params: nil, queryStrings: queryStrings, headers: nil)
        return Observable<[Activity]>.create { (observer) -> Disposable in
            Alamofire.request(request.request.url,
                              method: .get,
                              parameters: nil,
                              encoding: JSONEncoding.default,
                              headers: request.request.headers)
                .responseJSON(completionHandler: { (response ) in
                    KVLoading.hide()
                    
                    let alamofireResponse = AlamofireResponse.init(response: response)
                    if alamofireResponse.response.isSuccess {
                        var array : [Activity] = [Activity]()
                        if let value = response.result.value as? NSDictionary, let items = value.object(forKey: "Items") as? JSONArray {
                            array = Mapper<Activity>().mapArray(JSONArray: items)
                        }
                        
                        observer.onNext(array)
                        observer.onCompleted()
                    }else{
                        observer.onError(alamofireResponse.error!)
                        print(alamofireResponse.error!)
                        CRNotifications.showNotification(type: .error, title: "Error!", message: "Something went wrong...", dismissDelay: 3)
                    }
                    
                })
            return Disposables.create()
        }
        
    }
    
    func save<Activity: Mappable>(obj: Activity) -> Observable<ResponseType> {
       
        KVLoading.show()
        
        let request = ApiRequest.init(finalEndpoint: "activity")
        return Observable<ResponseType>.create { (observer) -> Disposable in
            Alamofire.request(request.request.url,
                              method: .post,
                              parameters: obj.toJSON(),
                              encoding: JSONEncoding.default,
                              headers: request.request.headers)
                .responseJSON(completionHandler: { (response) in
                    
                    KVLoading.hide()
                    
                    let alamofireResponse = AlamofireResponse.init(response: response)
                    if alamofireResponse.response.isSuccess {
                        observer.onNext(alamofireResponse.response)
                        observer.onCompleted()
                    }else{
                        observer.onError(alamofireResponse.error!)
                        print(alamofireResponse.error!)
                        CRNotifications.showNotification(type: .error, title: "Error!", message: "Something went wrong...", dismissDelay: 3)
                    }
                    
                })
            return Disposables.create()
        }
    }
    
    
    func delete<Activity: Mappable>(obj: Activity) -> Observable<ResponseType>{
        
        KVLoading.show()
        
        var param = Params()
        param.append(obj.toJSON()["id"] as! String)
        
        let request = ApiRequest.init(finalEndpoint: "activity", params: param, queryStrings: nil, headers: nil)
        return Observable<ResponseType>.create { (observer) -> Disposable in
            Alamofire.request(request.request.url,
                              method: .delete,
                              parameters: nil,
                              encoding: JSONEncoding.default,
                              headers: nil)
                .responseJSON(completionHandler: { (response) in
                    
                    KVLoading.hide()
                    
                    let alamofireResponse = AlamofireResponse.init(response: response)
                    if alamofireResponse.response.isSuccess {
                        observer.onNext(alamofireResponse.response)
                        observer.onCompleted()
                    }else{
                        observer.onError(alamofireResponse.error!)
                        print(alamofireResponse.error!)
                        CRNotifications.showNotification(type: .error, title: "Error!", message: "Something went wrong...", dismissDelay: 3)
                    }
                    
                })
            return Disposables.create()
        }
    }
    
}
