//
//  ServiceManager.swift
//  LDMA
//
//  Created by Alberto Galera Sánchez on 29/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import ObjectMapper
import Alamofire
import RxAlamofire
import RxCocoa
import RxSwift
import KVLoading
import CRNotifications


struct ServiceManagerAlamoFire: ListProtocol {
    
    func list<Service: Mappable>() -> Observable<[Service]> {
        KVLoading.show()
        let request = ApiRequest.init(finalEndpoint: "service")
        return Observable<[Service]>.create { (observer) -> Disposable in
            Alamofire.request(request.request.url,
                              method: .get,
                              parameters: nil,
                              encoding: JSONEncoding.default,
                              headers: request.request.headers)
                .responseJSON(completionHandler: { (response ) in
                    KVLoading.hide()
                
                    let alamofireResponse = AlamofireResponse.init(response: response)
                    if alamofireResponse.response.isSuccess {
                        var arrayService  : [Service] = []
                        if let objJson = response.result.value as! NSArray?{
                            for element in objJson {
                                let data = element as! NSDictionary
                                if let data = Mapper<Service>().map(JSONObject: data) {
                                    arrayService.append(data)
                                }
                               
                            }
                        }

                         observer.onNext(arrayService)
                        observer.onCompleted()
                    }else{
                        observer.onError(alamofireResponse.error!)
                        print(alamofireResponse.error!)
                        CRNotifications.showNotification(type: .error, title: "Error!", message: "Something went wrong...", dismissDelay: 3)
                    }
                    
                })
            return Disposables.create()
        }
   
    }
    
}

