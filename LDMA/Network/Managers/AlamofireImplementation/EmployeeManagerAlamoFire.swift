//
//  EmployeeManager.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 09/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import ObjectMapper
import Alamofire
import RxAlamofire
import RxCocoa
import RxSwift
import KVLoading
import CRNotifications

struct EmployeeManagerAlamoFire: ListProtocol {
    
    func list<Employee: Mappable>() -> Observable<[Employee]> {
        KVLoading.show()
        
        var queryStrings = QueryStrings()
        queryStrings["company_id"] = "1" //TODO: add employee company
        
        let request = ApiRequest.init(finalEndpoint: "employee", params: nil, queryStrings: queryStrings, headers: nil)
        return Observable<[Employee]>.create { (observer) -> Disposable in
            Alamofire.request(request.request.url,
                              method: .get,
                              parameters: nil,
                              encoding: JSONEncoding.default,
                              headers: request.request.headers)
                .responseJSON(completionHandler: { (response ) in
                    
                    KVLoading.hide()
                    
                    let alamofireResponse = AlamofireResponse.init(response: response)
                    if alamofireResponse.response.isSuccess {
                        var array : [Employee] = [Employee]()
                        if let value = response.result.value as? JSONArray {
                            array = Mapper<Employee>().mapArray(JSONArray: value)
                        }
                        
                        observer.onNext(array)
                        observer.onCompleted()
                    }else{
                        observer.onError(alamofireResponse.error!)
                        print(alamofireResponse.error!)
                        CRNotifications.showNotification(type: .error, title: "Error!", message: "Something went wrong...", dismissDelay: 3)
                    }
                    
                })
            return Disposables.create()
        }
    }
    
}
   
