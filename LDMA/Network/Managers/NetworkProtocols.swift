//
//  NetworkProtocols.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 12/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper


public protocol ListProtocol {
    
    func list<T: Mappable>() -> Observable<[T]>
    
}

public protocol SaveProtocol {
    
    func save<T: Mappable>(obj: T) -> Observable<ResponseType>
    
}

public protocol DeleteProtocol {
    
    func delete<T: Mappable>(obj: T) -> Observable<ResponseType>
    
}

public protocol NetworkResponse {
    
    var status: Int { get set }
    var response: ResponseType { get set }
    var error: Error? { get set }
    
}
