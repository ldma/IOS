//
//  NetworkCommons.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 13/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

public typealias ResponseType = (isSuccess: Bool, message: String?)
public typealias Params = [String]
public typealias QueryStrings = [String: String]
public typealias Headers = [String: String]

class ApiRequest {
    
    var finalEndpoint: String
    var params: Params?
    var queryStrings: QueryStrings?
    var addedHeaders: Headers?
    
    init(finalEndpoint: String, params: Params? = nil, queryStrings: QueryStrings? = nil, headers: Headers? = nil){
        self.finalEndpoint = finalEndpoint
        self.params = params
        self.queryStrings = queryStrings
        self.addedHeaders = headers
    }
    
    var request: (url: URL, headers: Headers) {
        
        var urlString = ""
        var headers = Headers()
        self.addedHeaders?.forEach({ (key, value) in
            headers[key] = value
        })
        
        var resourceFileDictionary: NSDictionary?
        if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
            resourceFileDictionary = NSDictionary(contentsOfFile: path)
        }
        
        if let resourceFileDictionaryContent = resourceFileDictionary {
            let Endpoint = resourceFileDictionaryContent.object(forKey: "Endpoint") as! JSONDictonary
            if let apiUrl = Endpoint["url"]  {
                urlString = "\(apiUrl)\(self.finalEndpoint)"
            }
            
            if self.params != nil {
                urlString = "\(urlString)/"
                self.params?.forEach({ (value) in
                    urlString = "\(urlString)\(value)/"
                })
            }
            
            if self.queryStrings != nil {
                urlString = "\(urlString)?"
                self.queryStrings?.forEach({ (key, value) in
                    urlString = "\(urlString)\(key)=\(value)&"
                })
            }
            
            if let contentType = Endpoint["Content-Type"]  {
                headers["Content-Type"] = "\(contentType)"
            }
        }
        
        let url = URL.init(string: urlString)!    
        return (url, headers)
        
    }
}






