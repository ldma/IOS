//
//  ActivityInteractor.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 14/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import ObjectMapper

public class ActivityListInteractor: ListInteractor {
    
    public func list() -> Observable<[Activity]> {
        return listManager.list()
    }
    
}

public class ActivitySaveInteractor: SaveInteractor {
    
    public func save(obj: Activity) -> Observable<ResponseType> {
        return saveManager.save(obj: obj)
    }
    
}

public class ActivityDeleteInteractor: DeleteInteractor {
    
    public func delete(obj: Activity) -> Observable<ResponseType> {
        return deleteManager.delete(obj: obj)
    }
    
}
