//
//  InteractorProtocols.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 14/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import RxSwift
import ObjectMapper

public class ListInteractor {
    
    let listManager: ListProtocol
    
    public init(manager: ListProtocol){
        self.listManager = manager
    }
    
}

public class SaveInteractor {
    
    let saveManager: SaveProtocol
    
    public init(manager: SaveProtocol){
        self.saveManager = manager
    }
    
}

public class DeleteInteractor {
    
    let deleteManager: DeleteProtocol
    
    public init(manager: DeleteProtocol){
        self.deleteManager = manager
    }
    
}
