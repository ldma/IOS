//
//  EmployeeInteractor.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 14/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift
import ObjectMapper

public class EmployeeListInteractor: ListInteractor {
    
    public func list() -> Observable<[Employee]> {
        return listManager.list()
    }
    
}
