import UIKit
import AWSCore
import AWSCognitoIdentityProvider


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    

    var window: UIWindow?
    


    var loginViewController: LoginViewController?
    var navigationController: UINavigationController?
    var storyboard: UIStoryboard? {
        return UIStoryboard(name: "Main", bundle: nil)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // setup logging
        AWSDDLog.sharedInstance.logLevel = .verbose
        AWSDDLog.add(AWSDDTTYLogger.sharedInstance)
    
        //setup cognito
        setupCognitoUserPool()
        
        //setup back button
        //setupBackButton()
        
        return true
    }
    
    func setupCognitoUserPool(){
        // Warn user if configuration not updated
        if (CognitoIdentityUserPoolId == "YOUR_USER_POOL_ID") {
            let alertController = UIAlertController(title: "Invalid Configuration",
                                                    message: "Please configure user pool constants in Constants.swift file.",
                                                    preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            alertController.addAction(okAction)
            
            self.window?.rootViewController!.present(alertController, animated: true, completion:  nil)
        }
        
        
        //fecth values
        
        let clientId = CognitoIdentityUserPoolAppClientId
        let poolId = CognitoIdentityUserPoolId
        let clientSecret = CognitoIdentityUserPoolAppClientSecret
        let region:AWSRegionType = CognitoIdentityUserPoolRegion
        
        //setup service configuration
        let serviceConfiguration = AWSServiceConfiguration(region: region, credentialsProvider: nil)
    
        //create pool configuration
        let poolConfiguration = AWSCognitoIdentityUserPoolConfiguration(clientId: clientId, clientSecret: clientSecret, poolId: poolId)
        
        //initialize user pool client
        AWSCognitoIdentityUserPool.register(with: serviceConfiguration, userPoolConfiguration: poolConfiguration, forKey: AWSCognitoUserPoolsLogInProviderKey)
        
        // fecth user pool
        let pool = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsLogInProviderKey)
        
        //set delegate
        pool.delegate = self
    
    }

    func setupBackButton() {        
        var backButtonImage = UIImage(named: "back_button_icon")
        backButtonImage = backButtonImage?.stretchableImage(withLeftCapWidth: 15, topCapHeight: 30)
        UIBarButtonItem.appearance().setBackButtonBackgroundImage(backButtonImage, for: .normal, barMetrics: .default)
    }
    
}

