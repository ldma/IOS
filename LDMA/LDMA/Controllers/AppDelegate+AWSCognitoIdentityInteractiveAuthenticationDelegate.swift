//
//  AppDelegate+AWSCognitoIdentityInteractiveAuthenticationDelegate.swift
//  LDMA
//
//  Created by Begoña Hormaechea on 24/9/17.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit
import AWSCognitoIdentityProvider

extension AppDelegate: AWSCognitoIdentityInteractiveAuthenticationDelegate {
    func startPasswordAuthentication() -> AWSCognitoIdentityPasswordAuthentication {

        if( self.navigationController == nil){
            self.navigationController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? UINavigationController
        }
        if( self.loginViewController == nil) {
            self.loginViewController = self.navigationController?.viewControllers[0] as? LoginViewController
        }

        DispatchQueue.main.async {
            self.navigationController!.popToRootViewController(animated: true)
            if(!self.navigationController!.isViewLoaded ||
                self.navigationController!.view.window == nil){
                self.window?.rootViewController?.present(self.navigationController!, animated: true, completion: nil)
            }
        }
        return self.loginViewController!
    }
    
//    func startPasswordAuthentication() -> AWSCognitoIdentityPasswordAuthentication {
//
//        if( self.navigationController == nil){
//            self.navigationController = self.window?.rootViewController as? UINavigationController
//
//        }
//        if( self.loginViewController == nil) {
//            self.loginViewController =
//                self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
//        }
//
//        DispatchQueue.main.async {
//            if(self.navigationController!.isViewLoaded ||
//                self.navigationController!.view.window == nil){
//                self.navigationController?.present(self.loginViewController!,animated: true, completion: nil)
//            }
//        }
//        return self.loginViewController!
//    }
}
