//
//  ListServiceCollectionViewCell.swift
//  LDMA
//
//  Created by Alberto Galera Sánchez on 04/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class ListServiceCollectionViewCell: UICollectionViewCell {
    

    
    @IBOutlet weak var nameService: UILabel!
    @IBOutlet weak var imageService: UIImageView!
    
    
}
