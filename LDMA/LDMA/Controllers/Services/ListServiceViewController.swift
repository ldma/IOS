//
//  ListServiceViewController.swift
//  LDMA
//
//  Created by Alberto Galera Sánchez on 29/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import Matisse

class ListServiceViewController: UIViewController{
    
    var arrayListService : [Service] = []
    var arrayFilteredService : [Service] = []
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var searchBarService: UISearchBar!
    @IBOutlet weak var collectionViewServices: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        let objArray : Observable<[Service]> = ServiceManagerAlamoFire().list()
        objArray.subscribe(onNext: { objects in
               self.arrayListService = objects
            }).disposed(by: disposeBag)
       
        let searchResults = searchBarService.rx.text.orEmpty
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .flatMapLatest { query -> Observable<[Service]> in
                if query.isEmpty {
                    return objArray
                }
                self.arrayFilteredService = self.arrayListService.filter { $0.name.hasPrefix(query) }
                for eachService in self.arrayFilteredService {
                    print("Filter by: \(query)\nmatches: \(eachService.name)")
                }
                return Observable.from(optional: self.arrayFilteredService)
                    .catchErrorJustReturn([])
            }
            .observeOn(MainScheduler.instance)
        
        searchResults.bind(to: collectionViewServices.rx.items(cellIdentifier: "cellId")){
            _ , service , cell in
           
            if let cellToUse = cell as? ListServiceCollectionViewCell{
              cellToUse.nameService.text = service.name
                Matisse.load((URL(string: service.thumb_url))!).showIn(cellToUse.imageService)
            }
        }.addDisposableTo(disposeBag)
    
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
