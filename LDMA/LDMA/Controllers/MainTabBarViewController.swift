import UIKit
import AWSCognitoIdentityProvider


class MainTabBarViewController: UITabBarController {

    var response: AWSCognitoIdentityUserGetDetailsResponse?
    var user: AWSCognitoIdentityUser?
    var pool: AWSCognitoIdentityUserPool?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchUserAttributes()  
    }

    func fetchUserAttributes() {
        self.pool = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsLogInProviderKey)
        if (self.user == nil){
            self.user = self.pool?.currentUser()
        }
        self.user?.getDetails().continueOnSuccessWith(block: { (task)-> Any? in
            DispatchQueue.main.async (execute: {
                // lo que se quiera hacer con los datos del usuario
                self.response = task.result
                //self.title = self.user?.username
            })
            return nil
        })
    }
}
