//
//  MapActivitiesViewController.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 17/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import UIKit
import MapKit
import RxCocoa
import RxSwift
import RxMapKit

class MapActivitiesViewController: UIViewController {
        
    @IBOutlet weak var mapView: MKMapView!
    
    var locManager:CLLocationManager!
    var locationList: [MapPin]?
    
    let disposeBag = DisposeBag()
    
    var activityList : [Activity]!

    override func viewDidLoad() {
        super.viewDidLoad()
        initMapPermissions()
        initAnnotations()
        initFetch()
        self.addBackgroundImage()
    }
    
    func initMapPermissions(){
        locManager = CLLocationManager()
        locManager.requestWhenInUseAuthorization()
        mapView.showsUserLocation = false
    }

    @IBAction func closeButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

}
