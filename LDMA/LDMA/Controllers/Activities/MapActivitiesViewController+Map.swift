//
//  MapActivitiesViewController+Map.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 17/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit
import RxMapKit
import MapKit

extension MapActivitiesViewController
{
    
    func initAnnotations() {
        self.mapView.rx.handleViewForAnnotation { (mapView, annotation) in
            if let _ = annotation as? MKUserLocation {
                return nil
            } else {
                return AnnotationView(annotation: annotation, mapView: mapView, reuseIdentifier: "pin_map")
            }
        }
    }
    
}
