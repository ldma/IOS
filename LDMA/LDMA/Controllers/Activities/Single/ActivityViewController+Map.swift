//
//  ActivityViewController+Map.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 16/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import MapKit

extension ActivityViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !annotation.isKind(of: MKUserLocation.classForCoder()) else { return nil }
        return AnnotationView(annotation: annotation, mapView: mapView, reuseIdentifier: "pin_map")
    }
    
}
