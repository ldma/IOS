//
//  EmployeeViewController.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 10/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import RxSwift
import RxCocoa
import CRNotifications

class ActivityViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var textTitle: UILabel!
    @IBOutlet weak var textOwner: UILabel!
    @IBOutlet weak var textInitialDate: UILabel!
    @IBOutlet weak var textEndDate: UILabel!
    @IBOutlet weak var textDescription: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var activity: Activity? = nil
    
    var locationList: [MapPin]?
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let _ = activity else{
            return
        }
        
        initView()
        initMap()
        self.addBackgroundImage()
    }
    
    func initView(){
        
        self.mapView.delegate = self
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.textTitle.text = self.activity!.name
        self.title = self.textTitle.text
        self.textOwner.text = self.activity!.owner.fullName
        self.textInitialDate.text = Date.backendFormat(date: self.activity!.initDate)
        self.textEndDate.text = Date.backendFormat(date: self.activity!.endDate)
        self.textDescription.text = self.activity!.description
    }
    
    func initMap(){
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        var activities = [Activity]()
        activities.append(activity!)
        
        self.locationList = Maps.populateLocationList(objects: activities)
        self.mapView = Maps.showLocationsCenteringRegion(map: mapView, locationList: locationList, padding: PlanActivityViewController.PADDING)
    }

    @IBAction func deleteButtonClicked(_ sender: Any) {
        
        ActivityDeleteInteractor(manager: ActivityManagerAlamoFire()).delete(obj: activity!).subscribe(
            onNext: { _ in
                self.navigationController?.popViewController(animated: true)
                CRNotifications.showNotification(type: .success, title: "Success!", message: "You successfully deleted the activity!", dismissDelay: 3)
            }, onError: { (error) in
                print(error)
                CRNotifications.showNotification(type: .error, title: "Error!", message: "Something went wrong...", dismissDelay: 3)
            }).disposed(by: disposeBag)
    }
    
}
