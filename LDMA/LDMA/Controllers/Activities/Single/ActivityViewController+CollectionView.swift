//
//  ActivityViewController+CollectionView.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 16/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

extension ActivityViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.activity!.invited.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "invitedCoworker", for: indexPath) as? InvitedCoworkerViewCell
        cell?.employee = self.activity!.invited[indexPath.item]
        return cell!
    }
    
}
