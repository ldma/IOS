//
//  MapActivitiesViewController+Fetch.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 17/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import MapKit
import RxCocoa
import RxSwift
import RxMapKit

extension MapActivitiesViewController {
    
    func initFetch(){
        
        self.mapView.rx.regionDidChange
            .subscribe(onNext: { coord in
                //TODO: Here the backend must calculate only te points in the region :(
                let obs : Observable<[Activity]> = ActivityListInteractor(manager: ActivityManagerAlamoFire()).list()
                obs.subscribe(onNext: { objects in
                    let locationList = Maps.populateLocationList(objects: objects)
                    self.mapView = Maps.showLocations(map: self.mapView, locationList: locationList)
                }).addDisposableTo(self.disposeBag)
            }).addDisposableTo(self.disposeBag)       
        
    }
    
}
