//
//  CoworkersViewController+FetchList.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 09/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// MARK - Fetch List
extension ActivitiesViewController {
    
    func initFetch(){       
                
        self.collectionView.delegate = nil
        self.collectionView.dataSource = nil
        
        searchBar.rx.text.orEmpty
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .flatMapLatest { query -> Observable<[Activity]> in
                if query.isEmpty {
                    let obs : Observable<[Activity]> = ActivityListInteractor(manager: ActivityManagerAlamoFire()).list()
                    obs.subscribe(onNext: { objects in
                        self.activityList = objects
                    }).disposed(by: self.disposeBag)
                    return obs
                }
                let q = query.lowercased().trimmingCharacters(in: .whitespaces)
                let filtered = self.activityList.filter({ (activity) -> Bool in
                    return activity.searchString.contains(q)
                })
                return Observable.from(optional: filtered).catchErrorJustReturn([])
            }
            .observeOn(MainScheduler.instance)
            .bind(to: collectionView.rx.items(cellIdentifier: "activityCellId")) { ( _ , activity, cell: ActivitiesViewCollectionViewCell) in
                cell.activity = activity
            }.addDisposableTo(disposeBag)
        
    }
    
}
