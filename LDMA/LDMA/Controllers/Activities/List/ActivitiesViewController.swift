//
//  DeleteActivityViewController.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 19/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import AWSCognitoIdentityProvider

class ActivitiesViewController: UIViewController {
    
    var user: AWSCognitoIdentityUser?
    var pool: AWSCognitoIdentityUserPool?
    var response: AWSCognitoIdentityUserGetDetailsResponse?
    
    var fetchArray : Observable<[Activity]>?
    var activityList : [Activity] = []

    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var searchBar = UISearchBar()
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
                
        self.pool = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsLogInProviderKey)
        if( self.user == nil){
            self.user = self.pool?.currentUser()
        }
        
        self.addBackgroundImage()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.initFetch()
    }
            
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = nil
        self.navigationController?.navigationBar.transparentNavigationBar()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureTabBarUI()
    }
    
    func configureTabBarUI() {
        self.title = "Activities"
        self.tabBarItem.image = UIImage(named: "activities_icon")
    }
    

    @IBAction func searchButtonClicked(_ sender: Any) {
        searchBar.placeholder = ""
        
        if navigationItem.titleView == searchBar {
            showLogo()
        }else{
            showSeachBar()
        }
    }
    
    
    
    // MARK: - Navigation
    
    @IBAction func logoutPressed(_ sender: Any) {
        self.user?.signOut()
        self.user?.getDetails().continueOnSuccessWith(block:{ (task) -> Any? in
            DispatchQueue.main.async {
                self.response = task.result
            }
            return nil
        })
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier
            {
            case "showActivity":
                let selectedIndex = self.collectionView.indexPathsForSelectedItems?.last?.item
                let activity = activityList[selectedIndex!]
                let vc = segue.destination as! ActivityViewController
                vc.activity = activity
                
            default:
                break
            }
        }
    }

}
