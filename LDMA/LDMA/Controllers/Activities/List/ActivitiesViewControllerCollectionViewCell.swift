//
//  CoworkersViewControllerCell.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 10/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

class ActivitiesViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    var _activity: Activity? = nil
    var activity: Activity{
        get{
            return _activity!
        }
        set{
            _activity = newValue
            
            nameLabel.text = activity.name
            imageView.mapFromLatLon(lat: activity.latitude, lon: activity.longitude)
            
        }
    }
    
}
