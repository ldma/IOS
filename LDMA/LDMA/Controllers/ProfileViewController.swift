import UIKit

class ProfileViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackgroundImage()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureTabBarUI()
    }
    
    func configureTabBarUI() {
        self.title = "Profile"
        self.tabBarItem.image = UIImage(named: "profile_icon")
    }

}
