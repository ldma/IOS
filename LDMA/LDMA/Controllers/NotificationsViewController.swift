import UIKit

class NotificationsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBackgroundImage()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureTabBarUI()
    }
    
    func configureTabBarUI() {
        self.title = "Notifications"
        self.tabBarItem.image = UIImage(named: "notification_icon")
    }
}
