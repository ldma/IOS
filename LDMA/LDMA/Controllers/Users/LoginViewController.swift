//
//  LoginViewController.swift
//  LDMA
//
//  Created by Begoña Hormaechea on 24/9/17.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit
import AWSCognitoIdentityProvider

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    
    
    var passwordAuthenticationCompletion: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>?
    
    var usernameText: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureScreen()
        self.addBackgroundImage()
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        if(self.usernameInput.text != nil && self.passwordInput.text != nil) {
            let authDetails = AWSCognitoIdentityPasswordAuthenticationDetails(username: self.usernameInput.text!, password: self.passwordInput.text!)
            self.passwordAuthenticationCompletion?.set(result: authDetails)
        }else{
            let alertController = UIAlertController(title: "Missing information",
                                                    message: "Please enter a valid user name and password",
                                                    preferredStyle: .alert)
            let retryAction = UIAlertAction(title: "Retry", style: .default, handler: nil)
            alertController.addAction(retryAction)
        }
        
    }
    
    // MARK: - Utilities
    func configureScreen() {
        self.title = "Login"
        self.cleanScreen()
    }
    
    func cleanScreen() {
        self.usernameInput.text = ""
        self.passwordInput.text = ""
    }
}
