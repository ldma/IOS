//
//  LoginViewController+AWSCognitoIdentityPasswordAuthentication.swift
//  LDMA
//
//  Created by Begoña Hormaechea on 24/9/17.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import AWSCognitoIdentityProvider

extension LoginViewController: AWSCognitoIdentityPasswordAuthentication{
    func getDetails(_ authenticationInput: AWSCognitoIdentityPasswordAuthenticationInput, passwordAuthenticationCompletionSource: AWSTaskCompletionSource<AWSCognitoIdentityPasswordAuthenticationDetails>) {
        self.passwordAuthenticationCompletion = passwordAuthenticationCompletionSource
        DispatchQueue.main.async {
            if( self.usernameText == nil){
                self.usernameText = authenticationInput.lastKnownUsername
            }
        }
    }
    
    func didCompleteStepWithError(_ error: Error?) {
        DispatchQueue.main.async {
            if let error = error as NSError? {
                let alertController = UIAlertController(title: error.userInfo["__type"] as? String,
                                                        message: error.userInfo["message"] as? String,
                                                        preferredStyle: .alert)
                let retryAction = UIAlertAction(title: "Retry",
                                                style: .default,
                                                handler: nil)
                alertController.addAction(retryAction)
                
                self.present(alertController, animated: true,completion: nil)
            } else {
                self.usernameInput.text = nil
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    
}
