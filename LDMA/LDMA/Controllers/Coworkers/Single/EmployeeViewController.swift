//
//  EmployeeViewController.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 10/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

class EmployeeViewController: UIViewController {
    
    var employee: Employee? = nil
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var employeeName: UILabel!
    @IBOutlet weak var employeeEmail: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let _ = employee else{
            return
        }
        
        employeeName.text = employee?.fullName
        imageView.imageFromServerURL(urlString: (employee?.fullAvatarUrl)!)
        
        if let email = employee?.email {
            employeeEmail.text = "Email: \(email)"
        }
        
        self.addBackgroundImage()
        
    }
    

}
