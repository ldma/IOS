//
//  DeleteActivityViewController.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 19/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CoworkersViewController: UIViewController {
    
    var fetchArray : Observable<[Employee]>?
    var employeeList : [Employee] = []
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var searchBar = UISearchBar()
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initFetch()
        self.addBackgroundImage()
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationItem.backBarButtonItem = nil
        self.navigationController?.navigationBar.transparentNavigationBar()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureTabBarUI()
    }
    
    func configureTabBarUI() {
        self.title = "Coworkers"
        self.tabBarItem.image = UIImage(named: "coworkers_icon")
    }
    
    @IBAction func searchButtonClicked(_ sender: Any) {        
        searchBar.placeholder = ""
        
        if navigationItem.titleView == searchBar {
            showLogo()
        }else{
            showSeachBar()
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier
            {
            case "showEmployee":
                let selectedIndex = self.collectionView.indexPathsForSelectedItems?.last?.item
                let employee = employeeList[selectedIndex!]
                let vc = segue.destination as! EmployeeViewController
                vc.employee = employee
                
            default:
                break
            }
        }
    }

}
