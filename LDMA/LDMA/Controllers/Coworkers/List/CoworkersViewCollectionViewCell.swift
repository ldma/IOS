//
//  CoworkersViewControllerCell.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 10/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

class CoworkersViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var _employee: Employee? = nil
    var employee: Employee{
        get{
            return _employee!
        }
        set{
            _employee = newValue
            
            nameLabel.text = employee.fullName
            imageView.imageFromServerURL(urlString: employee.fullAvatarUrl)
            
        }
    }
    
}
