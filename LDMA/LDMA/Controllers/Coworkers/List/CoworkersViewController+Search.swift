//
//  File.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 09/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import UIKit

// MARK: - UISearchBarDelegate

extension CoworkersViewController: UISearchBarDelegate{
    
    func showLogo(){
        self.navigationItem.titleView = nil
        
        self.searchBar.alpha = 1
        self.navigationItem.titleView?.alpha = 0
        
        self.searchBar.alpha = 0
        self.searchBar.text = ""
    }
    
    func showSeachBar(){
        navigationItem.setLeftBarButton(nil, animated: true)
        self.searchBar.alpha = 0
        navigationItem.titleView = searchBar
        
        self.searchBar.alpha = 1
        self.searchBar.becomeFirstResponder()
    }
    
}
