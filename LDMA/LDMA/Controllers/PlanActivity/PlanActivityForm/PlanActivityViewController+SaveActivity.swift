//
//  PlanActivityViewController+SaveActivity.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 14/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import CRNotifications

extension PlanActivityViewController {
    
    func saveActivity(){
        
        self.activity.invited = self.employeesSelected
        
        let obs : Observable<ResponseType> = ActivitySaveInteractor(manager: ActivityManagerAlamoFire()).save(obj: self.activity)
        obs.subscribe(onNext: { (response) in
            self.clearInputs()
            CRNotifications.showNotification(type: .success, title: "Success!", message: "You successfully created an activity!", dismissDelay: 3)
        }).disposed(by: self.disposeBag)
        
        
    }
    
}
