//
//  PlanActivityViewController.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 11/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import RxSwift
import RxCocoa
import CoreLocation

class PlanActivityViewController: UIViewController {
    
    static let PADDING = 1.0
    
    var activity: Activity!
    var employeesSelected: [Employee] = []
    
    @IBOutlet weak var textTitle: UITextField!
    @IBOutlet weak var textDescription: UITextField!
    @IBOutlet weak var textInitialDate: TouchableTextView!
    @IBOutlet weak var textEndDate: TouchableTextView!
    @IBOutlet weak var switchPrivate: UISwitch!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewMessage: UILabel!
    
    let disposeBag = DisposeBag()
    
    let initialDatePicker = UIDatePicker()
    let finalDatePicker = UIDatePicker()
    
    //Location
    var locManager: CLLocationManager!
    var location: CLLocation?
    var locationList: [MapPin]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureTabBarUI()
    }
    
    func configureTabBarUI() {
        self.title = "Plan Activity"
        self.tabBarItem.image = UIImage(named: "plan_icon")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initMapPermissions()
        initActivity()
        initMap()
        configureUI()
        configureListeners()
        self.addBackgroundImage()
    }
    
    // MARK: - Initializers
    
    func initMapPermissions(){
        locManager = CLLocationManager()
        locManager.delegate = self
        self.mapView.delegate = self
        locManager.requestWhenInUseAuthorization()        
        mapView.showsUserLocation = false
    }
    
    func initActivity(){
        self.activity = Activity.init()        
    }
    
    func initMap(){
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        var activities = [Activity]()
        activities.append(activity)
        
        self.locationList = Maps.populateLocationList(objects: activities)
        self.mapView = Maps.showLocationsCenteringRegion(map: mapView, locationList: locationList, padding: PlanActivityViewController.PADDING)
    }
    
    func configureUI() {
        self.view.backgroundColor = UIColor.init(hex: CONSTANTS.DEFAULT_BACKGROUND_COLOR)
        
        let toolBar = UIToolbar().ToolbarPiker(mySelect: #selector(PlanActivityViewController.dismissPicker))
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        self.collectionView.backgroundColor = UIColor.white
        
        self.textTitle.setBottomBorder(backgroundColor: CONSTANTS.DEFAULT_BACKGROUND_COLOR)
        self.textDescription.setBottomBorder(backgroundColor: CONSTANTS.DEFAULT_BACKGROUND_COLOR)
        self.textInitialDate.setBottomBorder(backgroundColor: CONSTANTS.DEFAULT_BACKGROUND_COLOR)
        self.textEndDate.setBottomBorder(backgroundColor: CONSTANTS.DEFAULT_BACKGROUND_COLOR)
        
        self.textInitialDate.inputView = initialDatePicker
        self.initialDatePicker.datePickerMode = .dateAndTime
        self.textInitialDate.inputAccessoryView = toolBar
        
        self.textEndDate.inputView = finalDatePicker
        self.finalDatePicker.datePickerMode = .dateAndTime
        self.textEndDate.inputAccessoryView = toolBar
        
        self.textInitialDate.text = Date.backendFormat(date: Date())
        self.textEndDate.text = Date.backendFormat(date: Date())
        self.initialDatePicker.date = Date()
        self.finalDatePicker.date = Date()
        
    }
    
    func clearInputs(){
        self.employeesSelected = []
        self.collectionView.reloadData()
        
        self.textTitle.text = ""
        self.textDescription.text = ""
        self.textInitialDate.text = Date.backendFormat(date: Date())
        self.textEndDate.text = Date.backendFormat(date: Date())
    }
    
    func configureListeners(){
        NotificationCenter.default.addObserver(self, selector: #selector(loadList), name: Notification.Name.coworkersCollection, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadMap), name: Notification.Name.mapView, object: nil)
        
        self.textTitle.rx.text.subscribe(onNext: { text in
            self.activity.name = text
            if let t = text, t.characters.count > 3 {
                self.navigationItem.rightBarButtonItem!.isEnabled = true
            }else{
                self.navigationItem.rightBarButtonItem!.isEnabled = false
            }
        }).addDisposableTo(disposeBag)
        
        self.textDescription.rx.text.subscribe(onNext: { text in
            self.activity.description = text
        }).addDisposableTo(disposeBag)
        
        self.textInitialDate.rx.value.subscribe(onNext: { date in
            self.activity.initDate = Date.dateFormat(s: date!)
        }).addDisposableTo(disposeBag)
        
        self.textEndDate.rx.value.subscribe(onNext: { date in
            self.activity.endDate = Date.dateFormat(s: date!)
        }).addDisposableTo(disposeBag)
        
        self.switchPrivate.rx.value.subscribe(onNext: { value in
            self.activity.isPrivate = value
        }).addDisposableTo(disposeBag)
        
        self.initialDatePicker.rx.date.subscribe(onNext: { date in
            self.handleInitialDatePicker()
        }).addDisposableTo(disposeBag)
        
        self.finalDatePicker.rx.date.subscribe(onNext: { date in
            self.handleFinalDatePicker()
        }).addDisposableTo(disposeBag)
        
        self.navigationItem.rightBarButtonItem!.rx.tap.subscribe (onNext: {
            self.saveActivity()
        }).addDisposableTo(disposeBag)
        
    }
    
    func loadList(){
        self.collectionView.reloadData()
    }
    
    func reloadMap(){
        self.initMap()
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            switch identifier
            {
            case "modalSelectCoworkers":
                let vc = segue.destination as! SelectCoworkersViewController
                vc.employeeListSelected = self.employeesSelected
                vc.delegate = self
            case "modalSelectLocation":
                let vc = segue.destination as! SelectLocationViewController
                vc.activity = self.activity
                vc.delegate = self
            default:
                break
            }
        }
    }

}
