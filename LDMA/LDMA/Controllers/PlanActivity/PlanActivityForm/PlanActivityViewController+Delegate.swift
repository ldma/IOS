//
//  PlanActivityViewController+Protocol.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 13/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
extension PlanActivityViewController : PlanActivityDelegate {
    
    func selectedEmployees(employees: [Employee]) {
        self.employeesSelected = employees
        NotificationCenter.default.post(name: Notification.Name.coworkersCollection, object: nil)
        self.collectionViewMessage.isHidden = self.employeesSelected.count > 0
    }
    
    func selectedLocation(activity: Activity) {
        self.activity = activity
        NotificationCenter.default.post(name: Notification.Name.mapView, object: nil)
    }
    
}

protocol PlanActivityDelegate {
    func selectedEmployees(employees: [Employee])
    func selectedLocation(activity: Activity)
}
