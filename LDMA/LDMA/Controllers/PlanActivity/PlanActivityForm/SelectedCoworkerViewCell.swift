//
//  PlanActivityViewController+CoworkerViewCellCoworker.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 13/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

class SelectedCoworkerViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var _employee: Employee? = nil
    var employee: Employee{
        get{
            return _employee!
        }
        set{
            _employee = newValue
            
            imageView.imageFromServerURL(urlString: employee.fullAvatarUrl)
            
        }
    }
    
}
