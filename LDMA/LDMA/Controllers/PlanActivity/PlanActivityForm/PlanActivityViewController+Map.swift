 //
//  PlanActivityViewController+Map.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 13/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit
import MapKit

extension PlanActivityViewController: MKMapViewDelegate, CLLocationManagerDelegate{
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !annotation.isKind(of: MKUserLocation.classForCoder()) else { return nil }
        return AnnotationView(annotation: annotation, mapView: mapView, reuseIdentifier: "pin_map")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    locManager.startUpdatingLocation()
                    self.activity.latitude = self.locManager.location?.coordinate.latitude
                    self.activity.longitude = self.locManager.location?.coordinate.longitude
                }
            }
        }
    }
    
}
