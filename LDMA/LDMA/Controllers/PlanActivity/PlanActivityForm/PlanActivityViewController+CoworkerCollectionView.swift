//
//  PlanActivityViewController+FetchList.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 13/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

extension PlanActivityViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return employeesSelected.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coworkerOnActivity", for: indexPath) as? SelectedCoworkerViewCell
        cell?.employee = employeesSelected[indexPath.item]
        return cell!
    }
    
}
