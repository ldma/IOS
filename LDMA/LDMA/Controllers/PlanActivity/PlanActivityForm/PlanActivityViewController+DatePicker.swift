//
//  PlanActivityViewController+DatePicker.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 12/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

extension PlanActivityViewController {
    
    func handleInitialDatePicker() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Date.MOBILE_TO_BACKEND
        textInitialDate.text = dateFormatter.string(from: initialDatePicker.date)
    }
    
    func handleFinalDatePicker() {
        
        if finalDatePicker.date < initialDatePicker.date {
            finalDatePicker.date = initialDatePicker.date
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = Date.MOBILE_TO_BACKEND
        textEndDate.text = dateFormatter.string(from: finalDatePicker.date)
    }
    
    func dismissPicker() {
        view.endEditing(true)
    }
    
}
