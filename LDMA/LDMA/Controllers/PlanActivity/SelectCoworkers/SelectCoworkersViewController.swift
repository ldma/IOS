//
//  SelectCoworkersViewController.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 12/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

class SelectCoworkersViewController: UIViewController {
    
    var fetchArray : Observable<[Employee]>?
    var employeeList : [Employee] = []
    var employeeListSelected: [Employee] = []
    var delegate: PlanActivityDelegate?
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initFetch()
        configureUI()
        self.addBackgroundImage()
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    func configureUI() {
        self.collectionView.allowsSelection = true
        self.collectionView.delegate = self
        
        self.view.backgroundColor = UIColor.init(hex: CONSTANTS.DEFAULT_BACKGROUND_COLOR)
        collectionView.backgroundColor = UIColor.init(hex: CONSTANTS.DEFAULT_BACKGROUND_COLOR)
        searchBar.backgroundImage = nil
        searchBar.isTranslucent = true
    }
    
    
}
