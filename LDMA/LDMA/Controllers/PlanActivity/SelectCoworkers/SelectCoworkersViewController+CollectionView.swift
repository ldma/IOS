//
//  SelectCoworkersViewController+CollectionView.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 12/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

extension SelectCoworkersViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedIndex = self.collectionView.indexPathsForSelectedItems?.last?.item
        let employee = self.employeeList[selectedIndex!]
        
        let notIncluded = self.employeeListSelected.filter() { $0.id == employee.id }.count == 0
        
        let cell = self.collectionView.cellForItem(at: indexPath) as? SelectCoworkersViewControllerViewCell
        
        if notIncluded {
            self.employeeListSelected.append(employee)
            cell?.checkedImage.isHidden = false
        }else{
            self.employeeListSelected = self.employeeListSelected.filter() { $0.id != employee.id }
            cell?.checkedImage.isHidden = true
        }
        
        self.delegate?.selectedEmployees(employees: self.employeeListSelected)
        
    }    
    
}
