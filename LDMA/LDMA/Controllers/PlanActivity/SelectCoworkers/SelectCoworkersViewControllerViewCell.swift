//
//  SelectCoworkersViewControllerViewCell.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 12/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import UIKit

class SelectCoworkersViewControllerViewCell: UICollectionViewCell {
    
    @IBOutlet weak var checkedImage: UIImageView!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nameText: UILabel!
    
    var _employee: Employee? = nil
    var employee: Employee{
        get{
            return _employee!
        }
        set{
            _employee = newValue
            
            nameText.text = employee.fullName
            avatarImage.imageFromServerURL(urlString: employee.fullAvatarUrl)
            
        }
    }
    
}
