//
//  SelectCoworkersViewController+Fetch.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 12/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// MARK - Fetch List
extension SelectCoworkersViewController {
    
    func initFetch(){
        
        searchBar.rx.text.orEmpty
            .throttle(0.3, scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .flatMapLatest { query -> Observable<[Employee]> in
                if query.isEmpty {
                    let obs : Observable<[Employee]> = EmployeeListInteractor(manager: EmployeeManagerAlamoFire()).list()
                    obs.subscribe(onNext: { objects in
                        self.employeeList = objects
                    }).disposed(by: self.disposeBag)
                    return obs
                }
                let q = query.lowercased().trimmingCharacters(in: .whitespaces)
                let filtered = self.employeeList.filter({ (employee) -> Bool in
                    return employee.searchString.contains(q)
                })
                return Observable.from(optional: filtered).catchErrorJustReturn([])
            }
            .observeOn(MainScheduler.instance)
            .bind(to: collectionView.rx.items(cellIdentifier: "coworkerToSelect")) { (_ , employee, cell: SelectCoworkersViewControllerViewCell) in
                cell.employee = employee
                
                cell.checkedImage.isHidden = self.employeeListSelected.filter({ (emp) -> Bool in
                    return emp.id == employee.id
                }).count == 0
                
            }.addDisposableTo(disposeBag)
        
    }
    
}
