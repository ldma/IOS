//
//  SelectLocationViewController.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 14/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import UIKit
import MapKit

class SelectLocationViewController: UIViewController {
    
    var activity: Activity? = nil
    
    @IBOutlet weak var mapView: MKMapView!
    
    var locManager:CLLocationManager!
    var locationList: [MapPin]?
    
    var gestureReconizer: UITapGestureRecognizer!
    
    var delegate: PlanActivityDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let _ = activity else{
            return
        }
        
        initMapPermissions()
        initMap()
        initGesture()
        self.addBackgroundImage()
    }
    
    func initMapPermissions(){
        locManager = CLLocationManager()
        locManager.delegate = self
        self.mapView.delegate = self
        locManager.requestWhenInUseAuthorization()
        mapView.showsUserLocation = false
    }
    
    func initMap(){
        self.mapView.removeAnnotations(self.mapView.annotations)
        
        var activities = [Activity]()
        activities.append(activity!)
        
        self.locationList = Maps.populateLocationList(objects: activities)
        self.mapView = Maps.showLocationsCenteringRegion(map: mapView, locationList: locationList, padding: PlanActivityViewController.PADDING)
    }
    
    func initGesture(){
        gestureReconizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.mapView.addGestureRecognizer(gestureReconizer)
    }
    
    func handleTap(_ sender: UITapGestureRecognizer) {
        
        let location = self.gestureReconizer!.location(in: mapView)
        let coordinate = mapView.convert(location,toCoordinateFrom: mapView)
        
        self.activity?.latitude = coordinate.latitude
        self.activity?.longitude = coordinate.longitude
        
        self.delegate?.selectedLocation(activity: self.activity!)
        
        initMap()
        
    }

    @IBAction func closeModalClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    

}
