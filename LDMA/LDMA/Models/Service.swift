//
//  Service.swift
//  LDMA
//
//  Created by Alberto Galera Sánchez on 29/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import ObjectMapper

class Service {
    
    var id: String!
    var name: String!
    var provider: String!
    var description: String!
    var thumb_url: String!
    var cover_url: String!
    var gps_lat : Double!
    var gps_long: Double!
    
    init(id: String?,
         name: String,
         provider: String,
         description: String,
         thumb_url: String,
         cover_url: String,
         gps_lat : Double,
         gps_long: Double) {
        self.id = id
        self.name = name
        self.provider = provider
        self.description = description
        self.thumb_url = thumb_url
        self.cover_url = cover_url
        self.gps_lat = gps_lat
        self.gps_long = gps_long
    }
    // Dummy initialization
    convenience init() {
        self.init(id: "1", name: "A", provider: "a", description: "b", thumb_url: "v", cover_url: "s", gps_lat: 3.3, gps_long: 4.4)
    }
    
    //Required by ObjectMapper
    required init?(map: Map) {}
    
   
    
}
