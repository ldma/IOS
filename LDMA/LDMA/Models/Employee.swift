//
//  Employee.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 09/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import ObjectMapper

public class Employee {
    
    var id: String!
    var company_id: String!
    var name: String!
    var surnames: String!
    var email: String!
    var avatar_url: String!
    
    init(id: String?, company_id: String, name: String, surnames: String, email: String, avatar_url: String){
        self.id = id == nil ? "0" : id
        self.company_id = company_id
        self.name = name
        self.surnames = surnames
        self.email = email
        self.avatar_url = avatar_url
    }
    
    convenience init(){
        self.init(id: "0",
                  company_id: "0",
                  name: "",
                  surnames: "",
                  email: "",
                  avatar_url: "")
    }
    
    var fullName : String{
        get {
            return "\(self.name!) \(self.surnames!)"
        }
    }
    
    var fullAvatarUrl : String{
        get {
            var resourceFileDictionary: NSDictionary?
            if let path = Bundle.main.path(forResource: "Info", ofType: "plist") {
                resourceFileDictionary = NSDictionary(contentsOfFile: path)
                if let resourceFileDictionaryContent = resourceFileDictionary {
                    if let s3Bucket = resourceFileDictionaryContent["S3Bucket"]  {
                        return "\(s3Bucket)\(self.avatar_url!)"
                    }
                }
            }
            return ""
        }
    }
    
    var searchString : String{
        get {
            return self.name.lowercased().trimmingCharacters(in: .whitespaces)
                + self.surnames.lowercased().trimmingCharacters(in: .whitespaces)
                + self.email.lowercased().trimmingCharacters(in: .whitespaces)
        }
    }
    
    //Required by ObjectMapper
    required public init?(map: Map) {}
    
    
}

