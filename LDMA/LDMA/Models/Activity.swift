//
//  Activity.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 12/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import ObjectMapper

public class Activity: LocalizableProtocol {
    
    public var id: String!
    public var company_id: String!
    public var owner_id: String!
    public var owner: Employee!
    public var service_id: String!
    public var name: String!
    public var description: String!
    public var isPrivate: Bool!
    public var init_date: String!
    public var end_date: String!
    public var invited: [Employee]!
    public var latitude: Double!
    public var longitude: Double!
    
    
    public var initDate : Date {
        get {
            if self.init_date != nil && self.init_date != "" {
                return Date.dateFormat(s: self.init_date)
            } else {
                return Date()
            }
        }
        set {
            self.init_date = Date.backendFormat(date: newValue)
        }
    }
    
    public var endDate : Date {
        get {
            if self.end_date != nil && self.end_date != "" {
                return Date.dateFormat(s: self.end_date)
            } else {
                return Date()
            }
        }
        set {
            self.end_date = Date.backendFormat(date: newValue)
        }
    }
    
    init(id: String?, company_id: String, owner_id: String, owner: Employee, service_id: String?, name: String, description: String, isPrivate: Bool, initDate: Date, endDate: Date, invited: [Employee], latitude: Double, longitude: Double){
        self.id = id == nil ? "0": id
        self.company_id = company_id
        self.owner_id = owner_id
        self.owner = owner
        self.service_id = service_id!
        self.name = name
        self.description = description
        self.isPrivate = isPrivate
        self.initDate = initDate
        self.endDate = endDate
        self.invited = invited
        self.latitude = latitude
        self.longitude = longitude
    }
    
    var searchString : String{
        get {
            return self.name.lowercased().trimmingCharacters(in: .whitespaces)
                + self.description.lowercased().trimmingCharacters(in: .whitespaces)
        }
    }
    
    //TODO: Modify company_id, owner_id to the real value
    convenience init(){
        self.init(id: nil,
                  company_id: "1",
                  owner_id: "6fc71ad0-a8a1-11e7-b55f-31d5144495b1",
                  owner: Employee(),
                  service_id: "0",
                  name: " ",
                  description: " ",
                  isPrivate: false,
                  initDate: Date(),
                  endDate: Date(),
                  invited: [Employee](),
                  latitude: CONSTANTS.DEFAULT_LATITUDE,
                  longitude: CONSTANTS.DEFAULT_LONGITUDE)
    }
    
    //Required by ObjectMapper 
    required public init?(map: Map) {}
    
    
}
