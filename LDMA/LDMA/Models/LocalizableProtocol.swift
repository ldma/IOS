//
//  Localizable.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 13/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation

public protocol LocalizableProtocol {
    
    var name: String! { get set }
    var latitude: Double! { get set }
    var longitude: Double! { get set }
    
}
