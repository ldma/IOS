import Foundation

struct CONSTANTS {
    
    static let DEFAULT_BACKGROUND_COLOR = "#ecf0f1"  //CLOUDS FLATUICOLORS.COM
    
    static let DEFAULT_LATITUDE: Double = 40.4113251
    static let DEFAULT_LONGITUDE: Double = -3.7191774

}
