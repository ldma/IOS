import Foundation

class Version {
    
    class func get() -> String{
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return "Version: \(version)"
        }
        return ""
    }
    
}
