import Foundation
import MapKit

public class Maps {
    
    //Load all annotations with location to the map
    class func populateLocationList<T: LocalizableProtocol>(objects: [T]) -> [MapPin]{
        var locationList = [MapPin]()
        for each in objects {
            let coordinate = CLLocationCoordinate2DMake(each.latitude, each.longitude)
            let mappin: MapPin = MapPin.init(coordinate: coordinate, title: each.name, subtitle: "")
            locationList.append(mappin)
        }
        return locationList
    }
    
    class func showLocations(map: MKMapView, locationList: [MapPin]?) -> MKMapView{
        map.removeAnnotations(map.annotations)
        if let locationList = locationList {
            if(locationList.count > 0){
                map.addAnnotations(locationList)
            }
        }
        return map
    }
    
    //Centering all locations in the region of the map 💪
    class func showLocationsCenteringRegion(map: MKMapView, locationList: [MapPin]?, padding: Double) -> MKMapView{
        
        let mapView = map
        
        if let locationList = locationList {
            if(locationList.count > 0){
                var topLeft = CLLocationCoordinate2D(latitude: -90, longitude: 180) //Top left of the world
                var bottomRight = CLLocationCoordinate2D(latitude: 80, longitude: -180) //Bottom right of the world
                
                //For all the locations find the most at left and the most at right
                for location in locationList {
                    topLeft.latitude = max(topLeft.latitude, location.coordinate.latitude)
                    topLeft.longitude = min(topLeft.longitude, location.coordinate.longitude)
                    bottomRight.latitude = min(bottomRight.latitude, location.coordinate.latitude)
                    bottomRight.longitude = max(bottomRight.longitude, location.coordinate.longitude)
                }
                
                //Get center of most left and right
                let centerLat = topLeft.latitude - (topLeft.latitude - bottomRight.latitude) / 2
                let centerLng = topLeft.longitude - (topLeft.longitude - bottomRight.longitude) / 2
                
                let center = CLLocationCoordinate2D(latitude: centerLat,longitude: centerLng)
                
                //Padding
                let deltaLat = abs(topLeft.latitude - bottomRight.latitude) * padding
                let deltaLng = abs(topLeft.longitude - bottomRight.longitude) * padding
                
                //creating span
                let span = MKCoordinateSpan.init(latitudeDelta: deltaLat, longitudeDelta: deltaLng)
                
                //creating region
                var region = MKCoordinateRegion(center: center, span: span)
                
                if locationList.count == 1 {
                    region = MKCoordinateRegionMakeWithDistance(center, 200, 200);
                }
                
                //creating map
                mapView.addAnnotations(locationList)
                mapView.regionThatFits(region)
                mapView.setRegion(region, animated: true)                
                
            }
            
        }
        
        return mapView
        
    }
    
}
