import Foundation
import MapKit

class AnnotationView: MKAnnotationView {
    
    init(annotation: MKAnnotation?, mapView: MKMapView, reuseIdentifier: String?) {
        
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        self.image = UIImage(named:"pin_map")
        
        if let annotation = annotation {
            self.annotation = annotation
        }
        self.canShowCallout = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
