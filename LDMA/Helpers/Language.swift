import Foundation

public func getPhoneLanguage() -> String {
    return Locale.current.languageCode!
}
