import Foundation

enum Errors : Error{
    case wrongUrlFormatForJSONResource
    case resourcePointedByUrlNotReachable
    case wrongJsonFormat
    case NotInLibrary
    case DBError
    case keyNotFound(String)
    case keyPathNotFound(String)
}
