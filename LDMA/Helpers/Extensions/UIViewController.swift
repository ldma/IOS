import UIKit

extension UIViewController {
    func addBackgroundImage() {
        let backgroundImage = UIImage.init(named: "logoSynergy")
        let backgroundImageView = UIImageView.init(frame: self.view.frame)
        
        backgroundImageView.image = backgroundImage
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.alpha = 0.1
        
        self.view.insertSubview(backgroundImageView, at: 0)
    }
}
