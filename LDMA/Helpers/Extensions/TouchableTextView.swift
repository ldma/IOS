//
//  TouchableTextView.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 12/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

class TouchableTextView : UITextField {
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }
    
    override func selectionRects(for range: UITextRange) -> [Any] {
        return []
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) || action == #selector(selectAll(_:)) || action == #selector(paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
}
