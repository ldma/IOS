//
//  UITextField.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 11/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func setBottomBorder(backgroundColor: String) {
        self.borderStyle = .none
        self.layer.backgroundColor = UIColor.init(hex: backgroundColor)?.cgColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.init(hex: "#bdc3c7")?.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}
