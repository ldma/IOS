//
//  Notification.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 13/10/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let coworkersCollection = Notification.Name(rawValue: "coworkersCollection")
    static let mapView = Notification.Name(rawValue: "mapView")
}
