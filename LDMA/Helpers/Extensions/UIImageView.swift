import Foundation
import UIKit

extension UIImageView {
    
    public func mapFromLatLon(lat: Double, lon: Double){
        let map_image_url = "http://maps.googleapis.com/maps/api/staticmap?center=<<LAT>>,<<LNG>>&zoom=17&size=150x150&scale=2&markers=%7Ccolor:0x9C7B14%7C<<LAT>>,<<LNG>>"
        let replaced = map_image_url.replacingOccurrences(of: "<<LAT>>", with: lat.description).replacingOccurrences(of: "<<LNG>>", with: lon.description)
        self.imageFromServerURL(urlString: replaced)
    }
    
    public func imageFromServerURL(urlString: String, placeholder: String = "placeholder") {
        
        let image = UIImage(named: placeholder)
        if(self.image == nil){
            self.image = image
        }
        
        if urlString != "" {
            DispatchQueue.global().async {
                do{
                    let d = try getFileFrom(urlString: urlString)
                    DispatchQueue.main.async {
                        
                        let image = UIImage(data: d)
                        
                        UIView.transition(with: self,
                                          duration: 0.4,
                                          options: .transitionCrossDissolve,
                                          animations: { self.image = image },
                                          completion: nil)
                    
                    }
                }catch{
                    
                }
            }
        }
        
    }
    
}
