import Foundation
import UIKit

extension UINavigationBar {
    
    func transparentNavigationBar() {
        self.setBackgroundImage(UIImage(), for: .default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
    }
    
    func appNavigationBar(){
        self.barTintColor = UIColor.black
        self.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.isTranslucent = false
    }
    
}

extension UINavigationItem {
    
    func setLogo(){
        self.title = "LDMA"
        self.titleView?.alpha = 1
    }
    
}
