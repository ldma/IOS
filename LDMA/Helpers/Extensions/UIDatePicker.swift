//
//  Datepicker.swift
//  LDMA
//
//  Created by Eric Risco de la Torre on 13/09/2017.
//  Copyright © 2017 LDMA. All rights reserved.
//

import Foundation
import UIKit

extension UIDatePicker {
    
    func datePickerValueChanged(sender:UIDatePicker) -> String {
        let dateFormatter = DateFormatter.init()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.none
        return dateFormatter.string(from: sender.date)
    }
    
}
