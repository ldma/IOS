# LDMA iOs APP

iOs APP For the Final Project of MOBILE STARTUP ENGINEERING BOOTCAMP IV

Devoloped with Swift3, RxSwift and Alamofire

![](https://cdn-images-1.medium.com/max/700/1*C7SZYZAu9Un_73Wh4uTPTA.png)

## Installation

First of all, install _Cocoapods_ if you don't have it installed yet.

```ruby 
sudo gem install cocoapods
```


Go to your project directory (where is located the _Podfile_) and run this command:

```ruby
pod install
```

## Keep in mind
Don't open __.xcodeproj__ file, open __xcworkspace__ instead. Otherwise, you will come across different compiling issues.

## TODO List

- Profile
- Services list
- Activity Infinite Scroll
- Activity Server Filtering
- Services Infinite Scroll
- Services Server Filtering
- Chat

## TEAM

- [Eric Risco][1]
- [Begoña Hormaechea][2]
- [Alberto Galera][3]
- [Paco Cardenal][4]
- [Eugenio Barquín][5]

[1]: https://github.com/eriscoand
[2]: https://github.com/begohorma
[3]: https://github.com/albertgs
[4]: https://github.com/pacocardenal
[5]: https://github.com/ebarquin

[10]:	https://s3.eu-west-2.amazonaws.com/com.ldma.frontend/index.html
